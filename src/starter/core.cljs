(ns starter.core
    (:require [reagent.core :as r] 
              [syn-antd.input :as input]))

(defonce greeting "hello")
(def who "world")


(defn app []
 (let [value (r/atom "alpha")]
   (fn [] 
     [:div 
      [:div greeting " " who] 
      [input/input {:value @value :onChange (fn [x] 
                                              (reset! value (.. x -target -value)))}]])))

(defn stop []
  (js/console.log "Stopping..."))

(defn start []
  (js/console.log "Starting...")
  (r/render [app]
            (.getElementById js/document "app")))

(defn ^:export init []
  (start))
